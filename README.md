perl\_version\_bump.vim
=======================

This filetype plugin for Perl code ("perl" filetype) provides buffer-local
mapping targets for normal mode to increment either the major or minor version
number of a package-scoped `$VERSION` assignment.

So with this format:

    our $VERSION = '1.23';

Keying a map to  `<Plug>(PerlVersionBumpMinor)` would yield:

    our $VERSION = '1.24';

Keying a map to `<Plug>(PerlVersionBumpMajor)` would yield:

    our $VERSION = '2.00';

There is no support for development versions with underscore prefixes like
`1.23_001` (yet).

License
-------

Copyright (c) [Tom Ryder][1].  Distributed under the same terms as Vim itself.
See `:help license`.

[1]: https://sanctum.geek.nz/
