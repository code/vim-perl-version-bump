" Version number specifier format
if exists('perl#version#bump#pattern')
  let s:pattern = perl#version#bump#pattern
else
  let s:pattern = '\m\C^'
        \ . '\(our\s\+\$VERSION\s*=\D*\)'
        \ . '\(\d\+\)\.\(\d\+\)'
        \ . '\(.*\)'
endif

" Helper function to format a number without decreasing its digit count
function! s:Format(old, new) abort
  return repeat('0', strlen(a:old) - strlen(a:new)).a:new
endfunction

" Version number bumper
function! s:Bump(major) abort
  let view = winsaveview()
  let li = search(s:pattern)
  if !li
    echomsg 'No version number declaration found'
    return
  endif
  let matches = matchlist(getline(li), s:pattern)
  let [lvalue, major, minor, rest]
        \ = matchlist(getline(li), s:pattern)[1:4]
  if a:major
    let major = s:Format(major, major + 1)
    let minor = s:Format(minor, 0)
  else
    let minor = s:Format(minor, minor + 1)
  endif
  let ver = major.'.'.minor
  call setline(li, lvalue.ver.rest)
  if a:major
    echomsg 'Bumped major $VERSION: '.ver
  else
    echomsg 'Bumped minor $VERSION: '.ver
  endif
  call winrestview(view)
endfunction

" Autoloaded interface functions
function! perl#version#bump#Major() abort
  call s:Bump(1)
endfunction
function! perl#version#bump#Minor() abort
  call s:Bump(0)
endfunction
